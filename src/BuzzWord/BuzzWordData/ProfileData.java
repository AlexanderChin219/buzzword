package BuzzWord.BuzzWordData;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by AlexChin on 11/12/16.
 */
public class ProfileData {
    private String name;
    private String password;
    private static boolean[] englishDictionary = new boolean[4];
    private static boolean[] nouns = new boolean[4];
    private static boolean[] verbs = new boolean[4];

//    create progress variables for each topic, indicating how far they are in each level

    public ProfileData() {

    }




    public ProfileData(String name, String password){
        this.name = name;
        this.password = password;
    }


    public ProfileData(File file) throws FileNotFoundException {
        Scanner s = new Scanner(file);

        this.name = s.nextLine().substring(5);
        this.password = s.nextLine().substring(9);

        String temp = s.nextLine().substring(3);
        String[] tempArray = temp.split(" ");

        for (int a = 0; a < englishDictionary.length; a++){
            if(tempArray[a].equalsIgnoreCase("true"))
                englishDictionary[a] = true;
        }

        temp = s.nextLine().substring(7);
        tempArray = temp.split(" ");

        for (int a = 0; a < nouns.length; a++){
            if(tempArray[a].equalsIgnoreCase("true"))
                nouns[a] = true;
        }

        temp = s.nextLine().substring(8);
        tempArray = temp.split(" ");

        for (int a = 0; a < verbs.length; a++){
            if(tempArray[a].equalsIgnoreCase("true"))
                verbs[a] = true;
        }

    }


    public void setEnglishDictionaryLevel(int level){
        englishDictionary[level] = true;
    }
    public void setPlacesLevel(int level){
        nouns[level] = true;
    }
    public void setAnimals(int level) {
        verbs[level] = true;}




    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean[] getEnglishDictionary() {
        return englishDictionary;
    }

    public boolean[] getNouns() {
        return nouns;
    }

    public boolean[] getVerbs() {
        return verbs;
    }
}
