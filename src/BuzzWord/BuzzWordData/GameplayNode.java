package BuzzWord.BuzzWordData;

import com.sun.javafx.geom.BaseBounds;
import com.sun.javafx.geom.transform.BaseTransform;
import com.sun.javafx.jmx.MXNodeAlgorithm;
import com.sun.javafx.jmx.MXNodeAlgorithmContext;
import com.sun.javafx.sg.prism.NGNode;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;

import java.awt.event.MouseMotionAdapter;

/**
 * Created by AlexChin on 12/9/16.
 */
public class GameplayNode extends Node {

    private int col;
    private int row;
    private String letter;
    private Circle circle = new Circle(50.0);

    public GameplayNode(int col, int row, Text letter){
        this.col = col;
        this.row = row;
        this.letter = letter.toString();

        StackPane stackPane = new StackPane();

        letter.setStyle("-fx-font-size: 50px");
        letter.setFill(Color.WHITE);
        stackPane.getChildren().addAll(circle, letter);

    }


    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }

    public String getLetter() {
        return letter;
    }

    public void MouseDrag(MouseEvent e){
//        if(e.)
    }





    @Override
    protected NGNode impl_createPeer() {
        return null;
    }

    @Override
    public BaseBounds impl_computeGeomBounds(BaseBounds bounds, BaseTransform tx) {
        return null;
    }

    @Override
    protected boolean impl_computeContains(double localX, double localY) {
        return false;
    }

    @Override
    public Object impl_processMXNode(MXNodeAlgorithm alg, MXNodeAlgorithmContext ctx) {
        return null;
    }
}
