package BuzzWord.Views;

import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;

/**
 * Created by AlexChin on 11/25/16.
 */
public class YesNoDialog extends Dialog {
    public YesNoDialog(String customText){
        Pane messageArea = new GridPane();
        Text message = new Text();
        message.setText(customText);
        messageArea.getChildren().addAll(message);

        getDialogPane().setContent(messageArea);
        getDialogPane().getButtonTypes().add(ButtonType.YES);
        getDialogPane().getButtonTypes().add(ButtonType.NO);
    }
}
