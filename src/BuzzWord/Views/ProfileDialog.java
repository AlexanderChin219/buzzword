package BuzzWord.Views;

import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * Created by AlexChin on 11/13/16.
 */

public class ProfileDialog extends Dialog{
    private final String name = "Profile Name";
    private final String password = "Profile Password";
    private TextField nameField;
    private PasswordField passwordField;


    public ProfileDialog(){
        GridPane namePass = new GridPane();

        nameField = new TextField();
        passwordField = new PasswordField();
        Label nameLabel = new Label(name);
        Label passwordLabel = new Label(password);



        namePass.add(nameLabel, 0, 0); //col row
        namePass.add(nameField, 1, 0);

        namePass.add(passwordLabel, 0, 1);
        namePass.add(passwordField, 1, 1);


        getDialogPane().setContent(namePass);
        getDialogPane().getButtonTypes().add(ButtonType.OK);
        getDialogPane().getButtonTypes().add(ButtonType.CANCEL);
    }

    public TextField getNameField() {
        return nameField;
    }

    public TextField getPasswordField() {
        return passwordField;
    }

    public String getNameFieldString(){
        return nameField.getCharacters().toString();
    }

    public String getPasswordFieldString(){
        return passwordField.getCharacters().toString();
    }


}
