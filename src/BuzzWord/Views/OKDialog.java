package BuzzWord.Views;

import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;

/**
 * Created by AlexChin on 11/28/16.
 */
public class OKDialog extends Dialog {
    public OKDialog(String customText){
        Pane messageArea = new GridPane();
        Text message = new Text();
        message.setText(customText);
        messageArea.getChildren().addAll(message);

        getDialogPane().setContent(messageArea);
        getDialogPane().getButtonTypes().add(ButtonType.OK);
    }
}
