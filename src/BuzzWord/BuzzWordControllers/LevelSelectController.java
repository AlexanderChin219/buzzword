package BuzzWord.BuzzWordControllers;

import BuzzWord.BuzzWordData.ProfileData;
import BuzzWord.Views.YesNoDialog;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import sun.java2d.cmm.Profile;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import static javafx.scene.paint.Color.*;

/**
 * Created by AlexChin on 11/12/16.
 */
public class LevelSelectController extends Controller implements Initializable {
    ProfileData data; //profile object that tracts progress in levels and information
    @FXML
    private GridPane levelGraphic;

    @FXML
    private Button home;

    @FXML
    private Label levelTitle;

    public void createLevelGraphic() {
        int levelInt = 1;
        for (int a = 0; a < 4; a++) {
            Button button = new Button();
            button.setStyle("-fx-background-color: transparent");
            button.setMinSize(60.0, 60.0);




            StackPane stackPane = new StackPane();
            Circle circle = new Circle(50.0);
            Text levelNumber;

            levelNumber = new Text((Integer.toString(levelInt)));

            //creating uncompleted default circles
            levelNumber.setStyle("-fx-font-size: 30px");
            levelNumber.setFill(WHITE);
            button.setVisible(false);


            if (levelIsComplete(levelInt)) {
                levelNumber.setFill(BLACK);
                circle.setFill(WHITE);
                button.setVisible(true);
            }

            int finalLevelInt = levelInt;
            button.setOnAction(event -> {
                try {
                    this.levelNumber = finalLevelInt;
                    gameplayLevelScreen();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });

            stackPane.getChildren().addAll(circle, levelNumber, button);
            levelGraphic.add(stackPane, a, 0);
            levelInt++;
        }
    }



    public void gameplayLevelScreen() throws IOException{

        Stage stage;
        Parent root;

        stage = (Stage) levelGraphic.getScene().getWindow();
        root = FXMLLoader.load(getClass().getResource("../Views/Gameplay.fxml"));

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    public void exit(ActionEvent event){
        YesNoDialog quit = new YesNoDialog("Would you like to quit?");
        quit.showAndWait()
                .filter(response -> response == ButtonType.YES)
                .ifPresent(LevelSelect -> {
                    System.exit(0);
                });
    }



    public void returnHomeScreen(ActionEvent event) throws IOException {
        Stage stage;
        Parent root;

        stage = (Stage) home.getScene().getWindow();
        root = FXMLLoader.load(getClass().getResource("../Views/Home.fxml"));

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }


    //    method that checks the profile data to see the progress
//    if true, change circle to white and text to white
    public boolean levelIsComplete(int levelInt) {
        if(selectedLevel.equalsIgnoreCase("English Dictionary"))
            return currentData.getEnglishDictionary()[levelInt-1];
        else if(selectedLevel.equalsIgnoreCase("Nouns"))
            return currentData.getNouns()[levelInt-1];
        else if(selectedLevel.equalsIgnoreCase("Verbs"))
            return currentData.getVerbs()[levelInt-1];
        else if(selectedLevel == null)
            return false;
        else
            return false;

    }

    public void createLevelGraphic(ProfileData data, String topic) {
//        create a method that initializes the level select based on the profile progress
    }

    public void createLevelTitle(){
        levelTitle.setText(selectedLevel);
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        createLevelGraphic();
        createLevelTitle();
    }
}
