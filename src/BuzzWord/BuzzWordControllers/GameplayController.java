package BuzzWord.BuzzWordControllers;

import BuzzWord.BuzzWordData.GameplayNode;
import BuzzWord.Views.YesNoDialog;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import javafx.scene.input.MouseEvent;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Array;
import java.net.URL;
import java.time.Clock;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by AlexChin on 11/12/16.
 */
public class GameplayController extends Controller implements Initializable {
    @FXML
    private GridPane gameplayGraphic;
    @FXML
    private Button levelSelect;
    @FXML
    private Button home;
    @FXML
    private Button exit;
    @FXML
    private Button playButton;
    @FXML
    private Label levelTitle;
    @FXML
    private Label levelNumberLabel;
    @FXML
    private Label targetScore;
    @FXML
    private Label secondsNumber;
    @FXML
    private Label currentGuessText;
    //updating based on drag and on typing


    private StackPane[] gameplayNodes = new StackPane[16];
    private long endTime;

    private boolean isPaused = false;
    private char[] alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();

    //Letters used to create the gameplay graphic
    private char[] gridLetters;
    private int targetScoreInt;

    private boolean[] selectedGridNodes = new boolean[16];

    public void play() {
//        endTime = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()) + 60 ;
//        secondsNumber.setText("60");
//        while(TimeUnit.MILLISECONDS.toSeconds((System.currentTimeMillis())) != endTime){
//
//        }


    }


    public void initializeGameplayGraphic() throws FileNotFoundException {

        int arrayCounter = 0;
        int counter = 0;
        //change path depending on the level selected...
        File dir = new File("src/rsc/dictionaries/ed.txt");
        Scanner s = new Scanner(dir);


        //
        ArrayList<String> dictionary = new ArrayList<String>();
        while (s.hasNextLine()) {
            dictionary.add(counter, s.nextLine());
            counter++;
        }

        int randomWordIndex = (int) (Math.random() * dictionary.size());
        String randomWord = dictionary.get(randomWordIndex);

        //cases for each level, word difficulty would go up

        while (randomWord.length() > 5 || randomWord.length() < 3) {
            randomWordIndex = (int) (Math.random() * dictionary.size());
            randomWord = dictionary.get(randomWordIndex);
        }
        dictionary.get(randomWordIndex);
        System.out.println(randomWord);
        targetScoreInt = randomWord.length() * 5;


        int choice = (int) (Math.random() * 2);

        if (choice == 0)
            gridLetters = horizontalRow(randomWord);
        else
            gridLetters = verticalRow(randomWord);


        for (int a = 0; a < 4; a++) {
            for (int b = 0; b < 4; b++) {
//                Text letter = new Text(Character.toString(gridLetters[arrayCounter]));
//                arrayCounter++;
//
//                GameplayNode node = new GameplayNode(a,b,letter);
//                gameplayGraphic.add(node, a, b);

                StackPane stackPane = new StackPane();
                Circle circle = new Circle(50.0);

                Text letter = new Text(Character.toString(gridLetters[arrayCounter]));

                letter.setStyle("-fx-font-size: 50px");
                letter.setFill(Color.WHITE);
                stackPane.getChildren().addAll(circle, letter);

                gameplayNodes[arrayCounter] = stackPane;

                arrayCounter++;
                gameplayGraphic.add(stackPane, a, b);
            }
        }


        setUpHandlers();
        initializeTargetScore();
        play();
    }

    public void setUpHandlers() {


        //gui changes
        for (int a = 0; a < gameplayNodes.length; a++) {
            int finalA = a;
            gameplayNodes[a].setOnDragDetected(e -> {
                Circle circ = (Circle) gameplayNodes[finalA].getChildren().get(0);
                circ.setFill(Color.GREEN);
                gameplayNodes[finalA].setMouseTransparent(true);
                gameplayNodes[finalA].startFullDrag();
                selectedGridNodes[finalA] = true;
            });

            gameplayNodes[a].setOnDragEntered(e -> {
                Circle circ = (Circle) gameplayNodes[finalA].getChildren().get(0);
                circ.setFill(Color.GREEN);
                gameplayNodes[finalA].setMouseTransparent(true);
                gameplayNodes[finalA].startFullDrag();
                selectedGridNodes[finalA] = true;

            });
            gameplayNodes[a].setOnMouseDragOver(e -> {
                Circle circ = (Circle) gameplayNodes[finalA].getChildren().get(0);
                circ.setFill(Color.GREEN);
                selectedGridNodes[finalA] = true;
            });
        }

        gameplayGraphic.setOnMouseDragReleased(e ->{
            for (int b = 0; b<selectedGridNodes.length; b++){
                if(selectedGridNodes[b] == true){
                    ((Circle)gameplayNodes[b].getChildren().get(0)).setFill(Color.BLACK);
                }
            }
        });


    }


    //        ((StackPane) gameplayGraphic.getChildren().get(a)).getChildren().get(1).setVisible(isShowing);




    public char[] horizontalRow(String randomWord) {
        char[] randomWordArray = randomWord.toUpperCase().toCharArray();
        char[] gridText = new char[16]; //off one?

        int horizontalLocation = (int) (Math.random() * 4);
        int counter = 0;

        for (int a = 0; a < gridText.length; a++) {
            if (a == horizontalLocation) {
                gridText[a] = randomWordArray[counter];
                counter++;

                horizontalLocation = (horizontalLocation + 4);
            } else {
                gridText[a] = alphabet[(int) (Math.random() * alphabet.length)];
            }
        }


        if (randomWord.length() == 5) {
            if (horizontalLocation == 16) {
                horizontalLocation = 13;
                for (int a = 0; a < gridText.length; a++) {
                    if (a == horizontalLocation) {
                        gridText[a] = randomWordArray[4];
                    }
                }
            }

            if (horizontalLocation == 17) {
                int randomPos = (int) (Math.random() * 2); //0,1 = left
                switch (randomPos) {
                    case 0:
                        horizontalLocation = 10;
                        break;
                    case 1:
                        horizontalLocation = 14;
                        break;
                    default:
                        break;
                }

                for (int a = 0; a < gridText.length; a++) {
                    if (a == horizontalLocation) {
                        gridText[a] = randomWordArray[4];
                    }
                }
            }


            if (horizontalLocation == 18) {
                int randomPos = (int) (Math.random() * 4);
                switch (randomPos) {
                    case 0:
                        horizontalLocation = 9;
                        break;
                    case 1:
                        horizontalLocation = 13;
                        break;
                    case 2:
                        horizontalLocation = 11;
                        break;
                    case 3:
                        horizontalLocation = 15;
                    default:
                        break;
                }
                for (int a = 0; a < gridText.length; a++) {
                    if (a == horizontalLocation) {
                        gridText[a] = randomWordArray[4];
                    }
                }
            }

            if (horizontalLocation == 19) {
                horizontalLocation = 14;
                for (int a = 0; a < gridText.length; a++) {
                    if (a == horizontalLocation) {
                        gridText[a] = randomWordArray[4];
                    }
                }
            }
        }

        for (int b = 0; b < gridText.length; b++) {
            System.out.print(gridText[b]);
        }

        return gridText;
    }

    public char[] verticalRow(String randomWord) {
        char[] randomWordArray = randomWord.toUpperCase().toCharArray();
        char[] gridText = new char[16];

        int verticalLocation = (int) (Math.random() * 4);

        switch (verticalLocation) {
            case 1:
                verticalLocation = 4;
                break;
            case 2:
                verticalLocation = 8;
                break;
            case 3:
                verticalLocation = 12;
                break;
            default:
                verticalLocation = 0;
                break;
        }


        int counter = 0;

        for (int a = 0; a < gridText.length; a++) {
            if (a == verticalLocation && counter < randomWordArray.length) {
                gridText[a] = randomWordArray[counter];
                counter++;
                verticalLocation++;
            } else {
                gridText[a] = alphabet[(int) (Math.random() * alphabet.length)];
            }
        }

        if (randomWord.length() == 5) {
            if (verticalLocation == 5) {
                verticalLocation = 7;
                for (int a = 0; a < gridText.length; a++) {
                    if (a == verticalLocation) {
                        gridText[a] = randomWordArray[4];
                    }
                }
            }

            if (verticalLocation == 9) {
                int randomPos = (int) (Math.random() * 4); //0,1 = left
                switch (randomPos) {
                    case 0:
                        verticalLocation = 6;
                        break;
                    case 1:
                        verticalLocation = 7;
                        break;
                    case 2:
                        verticalLocation = 14;
                        break;
                    case 3:
                        verticalLocation = 15;
                        break;
                    default:
                        break;
                }

                for (int a = 0; a < gridText.length; a++) {
                    if (a == verticalLocation) {
                        gridText[a] = randomWordArray[4];
                    }
                }
            }


            if (verticalLocation == 13) {
                int randomPos = (int) (Math.random() * 2);
                switch (randomPos) {
                    case 0:
                        verticalLocation = 10;
                        break;
                    case 1:
                        verticalLocation = 11;
                        break;
                    default:
                        break;
                }
                for (int a = 0; a < gridText.length; a++) {
                    if (a == verticalLocation) {
                        gridText[a] = randomWordArray[4];
                    }
                }
            }
        }


        return gridText;
    }

    public void exit(ActionEvent event) {

        YesNoDialog quit = new YesNoDialog("Would you like to quit?");

        showGrid(false);
        quit.showAndWait()
                .filter(response -> response == ButtonType.YES)
                .ifPresent(Home -> {
                    System.exit(0);
                });
        showGrid(true);
    }

    public void switchLevelSelect(ActionEvent event) throws IOException {
        Stage stage;
        Parent root;

        stage = (Stage) levelSelect.getScene().getWindow();
        root = FXMLLoader.load(getClass().getResource("../Views/LevelSelect.fxml"));

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    public void returnHomeScreen(ActionEvent event) throws IOException {
        Stage stage;
        Parent root;

        stage = (Stage) home.getScene().getWindow();
        root = FXMLLoader.load(getClass().getResource("../Views/Home.fxml"));

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    public void initializeLevelNumberLabel() {
        levelNumberLabel.setText("Level: " + Integer.toString(levelNumber));
    }

    public void initializeLevelTitleGraphic() {
        levelTitle.setText(selectedLevel);
    }

    public void initializeTargetScore() {
        targetScore.setText(Integer.toString(targetScoreInt));
    }

    public void pause(ActionEvent event) {
        if (isPaused) {
            isPaused = false;
            showGrid(true);
            playButton.setStyle("-fx-background-color: transparent");
        } else {
            isPaused = true;
            showGrid(false);
            playButton.setStyle("-fx-background-color: darkred");

        }

    }

    public void showGrid(boolean isShowing) {
        for (int a = 0; a < 16; a++) {
            ((StackPane) gameplayGraphic.getChildren().get(a)).getChildren().get(1).setVisible(isShowing);
        }
    }


    public void initializeTimerGraphic() throws InterruptedException {


    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            initializeGameplayGraphic();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        initializeLevelNumberLabel();
        initializeLevelTitleGraphic();
    }
}
