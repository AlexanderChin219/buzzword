package BuzzWord.BuzzWordControllers;

import BuzzWord.BuzzWordData.ProfileData;
import BuzzWord.Views.OKDialog;
import BuzzWord.Views.YesNoDialog;
import BuzzWord.Views.ProfileDialog;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Path;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Scanner;

/**
 * Created by AlexChin on 11/12/16.
 */
public class HomeController extends Controller implements Initializable {
    @FXML
    private GridPane buzzWordGraphic;

    @FXML
    private Button createNewProfile;
    @FXML
    private Button login;

    @FXML
    private Button logout;

    @FXML
    private ComboBox selectMode;
    @FXML
    private Button play;
    @FXML
    private Button exit;


    private String name;
    private String pass;
    private String txtFileName;

//    private String selectedLevel;


    public void newProfile(ActionEvent actionEvent) {


        ProfileDialog profile = new ProfileDialog();
        ProfileData data = new ProfileData();
        profile.showAndWait()
                .filter(response -> response == ButtonType.OK)
                .ifPresent(ProfileData -> {
                    data.setName(profile.getNameField().getText());
                    data.setPassword(profile.getPasswordField().getText());
                });

        data.setEnglishDictionaryLevel(0);
        data.setPlacesLevel(0);
        data.setAnimals(0);

        //create file...
        try {
            File file = new File(data.getName() + data.getPassword() + ".txt");


            PrintWriter printWriter = new PrintWriter("profiles/" + file);

            //write file
            printWriter.println("NAME:" + data.getName());
            printWriter.println("PASSWORD:" + data.getPassword());


            printWriter.print("ED:");
            for (int a = 0; a < data.getEnglishDictionary().length; a++) {
                printWriter.print(data.getEnglishDictionary()[a] + " ");
            }

            printWriter.println();
            printWriter.print("NOUNS:");
            for (int a = 0; a < data.getNouns().length; a++) {
                printWriter.print(data.getNouns()[a] + " ");
            }

            printWriter.println();
            printWriter.print("VERBS:");
            for (int a = 0; a < data.getVerbs().length; a++) {
                printWriter.print(data.getVerbs()[a] + " ");
            }

            printWriter.close();


//            System.out.println(file.getPath());
//            System.out.println(file.getAbsolutePath());

        } catch (IOException e) {

        }


    }

    public void login(ActionEvent actionEvent) {
        //search for data file...
        //cases: find file, change buttons
        //incorrect = popup.
        ProfileDialog loginDialog = new ProfileDialog();
        YesNoDialog incorrectLoginDialog = new YesNoDialog("Your profile does not exist.");


        File profileDir = new File("profiles/");
//        System.out.print(profileDir.getAbsoluteFile());
        File[] profilesArray = profileDir.listFiles();

//        //test
//        for (int a = 0; a < profilesArray.length; a++) {
//            System.out.println(profilesArray[a].getPath());
//        }


        //when correct login
        loginDialog.showAndWait()
                .filter(response -> response == ButtonType.OK)
                .ifPresent(Home -> {
                    name = loginDialog.getNameFieldString();
                    pass = loginDialog.getPasswordFieldString();
                    txtFileName = (name + pass) + ".txt";

                    for (int a = 0; a < profilesArray.length; a++) {
                        if (profilesArray[a].getPath().contains(txtFileName)) {
                            //change file into profiledataobject
                            try {
                                currentData = new ProfileData(profilesArray[a]);
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    if (currentData == null) {

                    } else {
                        createNewProfile.setVisible(false);
                        login.setVisible(false);
                        logout.setVisible(true);

                        selectMode.setVisible(true);
                        play.setVisible(true);
                        loggedIn = true;
                    }

                });


        if(currentData == null){
            OKDialog ok = new OKDialog("Invalid Login");
            ok.showAndWait();
        }

    }

    public void logout(ActionEvent actionEvent) {
        YesNoDialog logoutDialog = new YesNoDialog("Would you like to logout?");

        logoutDialog.showAndWait()
                .filter(response -> response == ButtonType.YES)
                .ifPresent(Home -> {
                    createNewProfile.setVisible(true);
                    login.setVisible(true);
                    logout.setVisible(false);
                    selectMode.setVisible(false);
                    play.setVisible(false);
                    currentData = null;
                    loggedIn = false;
                });


        //logout functionality, make it so the profile data is changed to a new profile or some dummy null
    }


    public void createBuzzWordGraphic() {
        String buzzWordString = "BZ  UZ    WR  OD";
        char[] buzzWordArray = buzzWordString.toCharArray();
        int counter = 0;
        for (int a = 0; a < 4; a++) {
            for (int b = 0; b < 4; b++) {
                StackPane stackPane = new StackPane();

                Circle circle = new Circle(50.0);
                Text letter = new Text(Character.toString(buzzWordArray[counter]));
                counter++;

                letter.setStyle("-fx-font-size: 30px");
                letter.setFill(Color.WHITE);
                stackPane.getChildren().addAll(circle, letter);
                buzzWordGraphic.add(stackPane, a, b);
            }
        }
    }

    //switch screens


    public void exit(ActionEvent event) {
        YesNoDialog quit = new YesNoDialog("Would you like to quit?");
        quit.showAndWait()
                .filter(response -> response == ButtonType.YES)
                .ifPresent(Home -> {
                    System.exit(0);
                });
    }


    public void switchLevelSelect(ActionEvent event) throws IOException {
        loggedIn = true;
        selectedLevel = (String) selectMode.getValue();
        OKDialog invalidOption = new OKDialog("Please select a level.");

        if(selectedLevel == null){
            invalidOption.showAndWait();
            return;
        }

        Stage stage;
        Parent root;

        stage = (Stage) play.getScene().getWindow();
        root = FXMLLoader.load(getClass().getResource("../Views/LevelSelect.fxml"));

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    public void loggedInScreen(){
        if(currentData != null){
            createNewProfile.setVisible(false);
            login.setVisible(false);
            logout.setVisible(true);
            selectMode.setVisible(true);
            play.setVisible(true);

        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        createBuzzWordGraphic();
        loggedInScreen();
    }
}
